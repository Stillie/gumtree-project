import 'package:gumtree_flutter_app/models/region_included.dart';

class ShipToLocations {
  List<RegionIncluded> regionIncluded;

  ShipToLocations({this.regionIncluded});

  ShipToLocations.fromJson(Map<String, dynamic> json) {
    if (json['regionIncluded'] != null) {
      regionIncluded = new List<RegionIncluded>();
      json['regionIncluded'].forEach((v) {
        regionIncluded.add(new RegionIncluded.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.regionIncluded != null) {
      data['regionIncluded'] = this.regionIncluded.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
