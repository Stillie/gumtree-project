class Seller {
  int feedbackScore;
  String username;
  String feedbackPercentage;

  Seller({this.feedbackScore, this.username, this.feedbackPercentage});

  Seller.fromJson(Map<String, dynamic> json) {
    feedbackScore = json['feedbackScore'];
    username = json['username'];
    feedbackPercentage = json['feedbackPercentage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['feedbackScore'] = this.feedbackScore;
    data['username'] = this.username;
    data['feedbackPercentage'] = this.feedbackPercentage;
    return data;
  }
}
