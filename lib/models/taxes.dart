import 'package:gumtree_flutter_app/models/tax_jurisdiction.dart';

class Taxes {
  TaxJurisdiction taxJurisdiction;
  String taxType;
  bool shippingAndHandlingTaxed;
  bool includedInPrice;
  bool ebayCollectAndRemitTax;

  Taxes({this.taxJurisdiction, this.taxType, this.shippingAndHandlingTaxed, this.includedInPrice, this.ebayCollectAndRemitTax});

  Taxes.fromJson(Map<String, dynamic> json) {
    taxJurisdiction = json['taxJurisdiction'] != null ? new TaxJurisdiction.fromJson(json['taxJurisdiction']) : null;
    taxType = json['taxType'];
    shippingAndHandlingTaxed = json['shippingAndHandlingTaxed'];
    includedInPrice = json['includedInPrice'];
    ebayCollectAndRemitTax = json['ebayCollectAndRemitTax'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taxJurisdiction != null) {
      data['taxJurisdiction'] = this.taxJurisdiction.toJson();
    }
    data['taxType'] = this.taxType;
    data['shippingAndHandlingTaxed'] = this.shippingAndHandlingTaxed;
    data['includedInPrice'] = this.includedInPrice;
    data['ebayCollectAndRemitTax'] = this.ebayCollectAndRemitTax;
    return data;
  }
}
