import 'package:gumtree_flutter_app/models/return_terms.dart';
import 'package:gumtree_flutter_app/models/seller.dart';
import 'package:gumtree_flutter_app/models/ship_to_locations.dart';
import 'package:gumtree_flutter_app/models/shipping_options.dart';
import 'package:gumtree_flutter_app/models/taxes.dart';

import 'estimated_availabilities.dart';
import 'item_image.dart';
import 'item_location.dart';
import 'item_price.dart';
import 'localized_aspects.dart';

class ItemResponse {
  String itemId;
  String title;
  Price price;
  String categoryPath;
  String condition;
  String conditionId;
  ItemLocation itemLocation;
  ItemImage image;
  List<ItemImage> additionalImages;
  String brand;
  Seller seller;
  String mpn;
  List<EstimatedAvailabilities> estimatedAvailabilities;
  List<ShippingOptions> shippingOptions;
  ShipToLocations shipToLocations;
  ReturnTerms returnTerms;
  List<Taxes> taxes;
  List<LocalizedAspects> localizedAspects;
  bool topRatedBuyingExperience;
  List<String> buyingOptions;
  String itemWebUrl;
  String description;
  bool enabledForGuestCheckout;
  bool eligibleForInlineCheckout;
  int lotSize;
  bool adultOnly;
  String categoryId;

  ItemResponse(
      {this.itemId,
      this.title,
      this.price,
      this.categoryPath,
      this.condition,
      this.conditionId,
      this.itemLocation,
      this.image,
      this.additionalImages,
      this.brand,
      this.seller,
      this.mpn,
      this.estimatedAvailabilities,
      this.shippingOptions,
      this.shipToLocations,
      this.returnTerms,
      this.taxes,
      this.localizedAspects,
      this.topRatedBuyingExperience,
      this.buyingOptions,
      this.itemWebUrl,
      this.description,
      this.enabledForGuestCheckout,
      this.eligibleForInlineCheckout,
      this.lotSize,
      this.adultOnly,
      this.categoryId});

  ItemResponse.fromJson(Map<String, dynamic> json) {
    itemId = json['itemId'];
    title = json['title'];
    price = json['price'] != null ? new Price.fromJson(json['price']) : null;
    categoryPath = json['categoryPath'];
    condition = json['condition'];
    conditionId = json['conditionId'];
    itemLocation = json['itemLocation'] != null ? new ItemLocation.fromJson(json['itemLocation']) : null;
    image = json['image'] != null ? new ItemImage.fromJson(json['image']) : null;
    if (json['additionalImages'] != null) {
      additionalImages = new List<ItemImage>();
      json['additionalImages'].forEach((v) {
        additionalImages.add(new ItemImage.fromJson(v));
      });
    }
    brand = json['brand'];
    seller = json['seller'] != null ? new Seller.fromJson(json['seller']) : null;
    mpn = json['mpn'];
    if (json['estimatedAvailabilities'] != null) {
      estimatedAvailabilities = new List<EstimatedAvailabilities>();
      json['estimatedAvailabilities'].forEach((v) {
        estimatedAvailabilities.add(new EstimatedAvailabilities.fromJson(v));
      });
    }
    if (json['shippingOptions'] != null) {
      shippingOptions = new List<ShippingOptions>();
      json['shippingOptions'].forEach((v) {
        shippingOptions.add(new ShippingOptions.fromJson(v));
      });
    }
    shipToLocations = json['shipToLocations'] != null ? new ShipToLocations.fromJson(json['shipToLocations']) : null;
    returnTerms = json['returnTerms'] != null ? new ReturnTerms.fromJson(json['returnTerms']) : null;
    if (json['taxes'] != null) {
      taxes = new List<Taxes>();
      json['taxes'].forEach((v) {
        taxes.add(new Taxes.fromJson(v));
      });
    }
    if (json['localizedAspects'] != null) {
      localizedAspects = new List<LocalizedAspects>();
      json['localizedAspects'].forEach((v) {
        localizedAspects.add(new LocalizedAspects.fromJson(v));
      });
    }
    topRatedBuyingExperience = json['topRatedBuyingExperience'];
    buyingOptions = json['buyingOptions'].cast<String>();
    itemWebUrl = json['itemWebUrl'];
    description = json['description'];
    enabledForGuestCheckout = json['enabledForGuestCheckout'];
    eligibleForInlineCheckout = json['eligibleForInlineCheckout'];
    lotSize = json['lotSize'];
    adultOnly = json['adultOnly'];
    categoryId = json['categoryId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['itemId'] = this.itemId;
    data['title'] = this.title;
    if (this.price != null) {
      data['price'] = this.price.toJson();
    }
    data['categoryPath'] = this.categoryPath;
    data['condition'] = this.condition;
    data['conditionId'] = this.conditionId;
    if (this.itemLocation != null) {
      data['itemLocation'] = this.itemLocation.toJson();
    }
    if (this.image != null) {
      data['image'] = this.image.toJson();
    }
    if (this.additionalImages != null) {
      data['additionalImages'] = this.additionalImages.map((v) => v.toJson()).toList();
    }
    data['brand'] = this.brand;
    if (this.seller != null) {
      data['seller'] = this.seller.toJson();
    }
    data['mpn'] = this.mpn;
    if (this.estimatedAvailabilities != null) {
      data['estimatedAvailabilities'] = this.estimatedAvailabilities.map((v) => v.toJson()).toList();
    }
    if (this.shippingOptions != null) {
      data['shippingOptions'] = this.shippingOptions.map((v) => v.toJson()).toList();
    }
    if (this.shipToLocations != null) {
      data['shipToLocations'] = this.shipToLocations.toJson();
    }
    if (this.returnTerms != null) {
      data['returnTerms'] = this.returnTerms.toJson();
    }
    if (this.taxes != null) {
      data['taxes'] = this.taxes.map((v) => v.toJson()).toList();
    }
    if (this.localizedAspects != null) {
      data['localizedAspects'] = this.localizedAspects.map((v) => v.toJson()).toList();
    }
    data['topRatedBuyingExperience'] = this.topRatedBuyingExperience;
    data['buyingOptions'] = this.buyingOptions;
    data['itemWebUrl'] = this.itemWebUrl;
    data['description'] = this.description;
    data['enabledForGuestCheckout'] = this.enabledForGuestCheckout;
    data['eligibleForInlineCheckout'] = this.eligibleForInlineCheckout;
    data['lotSize'] = this.lotSize;
    data['adultOnly'] = this.adultOnly;
    data['categoryId'] = this.categoryId;
    return data;
  }
}
