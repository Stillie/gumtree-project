class RegionIncluded {
  String regionName;
  String regionType;

  RegionIncluded({this.regionName, this.regionType});

  RegionIncluded.fromJson(Map<String, dynamic> json) {
    regionName = json['regionName'];
    regionType = json['regionType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['regionName'] = this.regionName;
    data['regionType'] = this.regionType;
    return data;
  }
}
