import 'item_price.dart';

class ShippingOptions {
  String shippingCostType;
  Price shippingCost;

  ShippingOptions({this.shippingCostType, this.shippingCost});

  ShippingOptions.fromJson(Map<String, dynamic> json) {
    shippingCostType = json['shippingCostType'];
    shippingCost = json['shippingCost'] != null ? new Price.fromJson(json['shippingCost']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['shippingCostType'] = this.shippingCostType;
    if (this.shippingCost != null) {
      data['shippingCost'] = this.shippingCost.toJson();
    }
    return data;
  }
}
