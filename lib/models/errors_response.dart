import 'package:equatable/equatable.dart';

import 'error.dart';

class ErrorsResponse extends Equatable {
  List<Error> errors;

  ErrorsResponse({this.errors});

  ErrorsResponse.fromJson(Map<String, dynamic> json) {
    if (json['errors'] != null) {
      errors = new List<Error>();
      json['errors'].forEach((v) {
        errors.add(new Error.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errors != null) {
      data['errors'] = this.errors.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
