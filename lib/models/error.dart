class Error {
  int errorId;
  String domain;
  String category;
  String message;
  String longMessage;

  Error({this.errorId, this.domain, this.category, this.message, this.longMessage});

  Error.fromJson(Map<String, dynamic> json) {
    errorId = json['errorId'];
    domain = json['domain'];
    category = json['category'];
    message = json['message'];
    longMessage = json['longMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorId'] = this.errorId;
    data['domain'] = this.domain;
    data['category'] = this.category;
    data['message'] = this.message;
    data['longMessage'] = this.longMessage;
    return data;
  }
}
