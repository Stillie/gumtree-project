import 'package:gumtree_flutter_app/models/region_included.dart';

class TaxJurisdiction {
  RegionIncluded region;
  String taxJurisdictionId;

  TaxJurisdiction({this.region, this.taxJurisdictionId});

  TaxJurisdiction.fromJson(Map<String, dynamic> json) {
    region = json['region'] != null ? new RegionIncluded.fromJson(json['region']) : null;
    taxJurisdictionId = json['taxJurisdictionId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    data['taxJurisdictionId'] = this.taxJurisdictionId;
    return data;
  }
}
