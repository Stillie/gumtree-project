class EstimatedAvailabilities {
  List<String> deliveryOptions;
  String estimatedAvailabilityStatus;
  int estimatedAvailableQuantity;
  int estimatedSoldQuantity;

  EstimatedAvailabilities({this.deliveryOptions, this.estimatedAvailabilityStatus, this.estimatedAvailableQuantity, this.estimatedSoldQuantity});

  EstimatedAvailabilities.fromJson(Map<String, dynamic> json) {
    deliveryOptions = json['deliveryOptions'].cast<String>();
    estimatedAvailabilityStatus = json['estimatedAvailabilityStatus'];
    estimatedAvailableQuantity = json['estimatedAvailableQuantity'];
    estimatedSoldQuantity = json['estimatedSoldQuantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deliveryOptions'] = this.deliveryOptions;
    data['estimatedAvailabilityStatus'] = this.estimatedAvailabilityStatus;
    data['estimatedAvailableQuantity'] = this.estimatedAvailableQuantity;
    data['estimatedSoldQuantity'] = this.estimatedSoldQuantity;
    return data;
  }
}
