import 'package:gumtree_flutter_app/common/base_model.dart';

import 'item_summary.dart';

class SearchResponse extends BaseModel {
  String href;
  int total;
  String next;
  int limit;
  int offset;
  List<ItemSummaries> itemSummaries;

  SearchResponse({this.href, this.total, this.next, this.limit, this.offset, this.itemSummaries});

  SearchResponse.fromJson(Map<String, dynamic> json) {
    href = json['href'];
    hasFailed = false;
    errorCode = 200;
    errorMessage = "Success";
    total = json['total'];
    next = json['next'];
    limit = json['limit'];
    offset = json['offset'];
    if (json['itemSummaries'] != null) {
      itemSummaries = new List<ItemSummaries>();
      json['itemSummaries'].forEach((v) {
        itemSummaries.add(new ItemSummaries.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['href'] = this.href;
    data['total'] = this.total;
    data['next'] = this.next;
    data['limit'] = this.limit;
    data['offset'] = this.offset;
    if (this.itemSummaries != null) {
      data['itemSummaries'] = this.itemSummaries.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
