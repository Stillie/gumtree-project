import 'package:gumtree_flutter_app/models/item_response.dart';
import 'package:gumtree_flutter_app/models/shipping_options.dart';

import 'categories.dart';
import 'item_image.dart';
import 'item_location.dart';
import 'item_price.dart';
import 'seller.dart';

class ItemSummaries extends ItemResponse {
  String itemHref;
  List<Categories> categories;

  ItemSummaries(
      {itemId,
      title,
      price,
      this.itemHref,
      seller,
      condition,
      conditionId,
      shippingOptions,
      buyingOptions,
      itemWebUrl,
      itemLocation,
      this.categories,
      adultOnly,
      image,
      additionalImages});

  ItemSummaries.fromJson(Map<String, dynamic> json) {
    itemId = json['itemId'];
    title = json['title'];
    price = json['price'] != null ? new Price.fromJson(json['price']) : null;
    itemHref = json['itemHref'];
    seller = json['seller'] != null ? new Seller.fromJson(json['seller']) : null;
    condition = json['condition'];
    conditionId = json['conditionId'];
    if (json['shippingOptions'] != null) {
      shippingOptions = new List<ShippingOptions>();
      json['shippingOptions'].forEach((v) {
        shippingOptions.add(new ShippingOptions.fromJson(v));
      });
    }
    buyingOptions = json['buyingOptions'].cast<String>();
    itemWebUrl = json['itemWebUrl'];
    itemLocation = json['itemLocation'] != null ? new ItemLocation.fromJson(json['itemLocation']) : null;
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    adultOnly = json['adultOnly'];
    image = json['image'] != null ? new ItemImage.fromJson(json['image']) : null;
    if (json['additionalImages'] != null) {
      additionalImages = new List<ItemImage>();
      json['additionalImages'].forEach((v) {
        additionalImages.add(new ItemImage.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['itemId'] = this.itemId;
    data['title'] = this.title;
    if (this.price != null) {
      data['price'] = this.price.toJson();
    }
    data['itemHref'] = this.itemHref;
    if (this.seller != null) {
      data['seller'] = this.seller.toJson();
    }
    data['condition'] = this.condition;
    data['conditionId'] = this.conditionId;
    if (this.shippingOptions != null) {
      data['shippingOptions'] = this.shippingOptions.map((v) => v.toJson()).toList();
    }
    data['buyingOptions'] = this.buyingOptions;
    data['itemWebUrl'] = this.itemWebUrl;
    if (this.itemLocation != null) {
      data['itemLocation'] = this.itemLocation.toJson();
    }
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['adultOnly'] = this.adultOnly;
    if (this.image != null) {
      data['image'] = this.image.toJson();
    }
    if (this.additionalImages != null) {
      data['additionalImages'] = this.additionalImages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
