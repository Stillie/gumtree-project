import 'package:gumtree_flutter_app/models/return_period.dart';

class ReturnTerms {
  bool returnsAccepted;
  String refundMethod;
  String returnShippingCostPayer;
  ReturnPeriod returnPeriod;

  ReturnTerms({this.returnsAccepted, this.refundMethod, this.returnShippingCostPayer, this.returnPeriod});

  ReturnTerms.fromJson(Map<String, dynamic> json) {
    returnsAccepted = json['returnsAccepted'];
    refundMethod = json['refundMethod'];
    returnShippingCostPayer = json['returnShippingCostPayer'];
    returnPeriod = json['returnPeriod'] != null ? new ReturnPeriod.fromJson(json['returnPeriod']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['returnsAccepted'] = this.returnsAccepted;
    data['refundMethod'] = this.refundMethod;
    data['returnShippingCostPayer'] = this.returnShippingCostPayer;
    if (this.returnPeriod != null) {
      data['returnPeriod'] = this.returnPeriod.toJson();
    }
    return data;
  }
}
