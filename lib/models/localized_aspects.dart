class LocalizedAspects {
  String type;
  String name;
  String value;

  LocalizedAspects({this.type, this.name, this.value});

  LocalizedAspects.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
