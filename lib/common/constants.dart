import 'dart:ui';

const Color kGREEN = Color(0xff72ef36);
const Color kGRAYISH_VIOLET = Color(0xff3b3141);
const Color kLIGHT_GRAYISH_ORANGE = Color(0xfff0ece6);
