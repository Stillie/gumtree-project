import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gumtree_flutter_app/common/constants.dart';

class GumtreeAppBar extends StatelessWidget {
  final Widget child;
  final String title;

  GumtreeAppBar({this.title, this.child});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      minimum: EdgeInsets.all(8),
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
          backgroundColor: kGRAYISH_VIOLET,
        ),
        body: Padding(
          child: child,
          padding: EdgeInsets.all(8),
        ),
      ),
    );
  }
}
