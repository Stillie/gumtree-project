import 'package:gumtree_flutter_app/models/errors_response.dart';

abstract class BaseModel extends ErrorsResponse {
  String errorMessage = "Failed";
  int errorCode = 999;
  bool hasFailed = true;

  BaseModel({this.errorMessage, this.errorCode, this.hasFailed});
}
