import 'package:gumtree_flutter_app/models/item_image.dart';
import 'package:gumtree_flutter_app/models/item_summary.dart';

String findAnImage(ItemSummaries itemSummaries) {
  if (itemSummaries != null) {
    ItemImage image = itemSummaries.image;
    if (image != null) {
      if (image.imageUrl != null && image.imageUrl.isNotEmpty) {
        return image.imageUrl;
      }
    } else {
      if (itemSummaries.additionalImages != null && itemSummaries.additionalImages.isNotEmpty) {
        for (ItemImage image in itemSummaries.additionalImages) {
          if (image != null && image.imageUrl != null && image.imageUrl.isNotEmpty) {
            return image.imageUrl;
          }
        }
      }
    }
  }
  return "";
}
