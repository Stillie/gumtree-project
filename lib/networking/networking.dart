import 'dart:convert';
import 'dart:io';

import 'package:gumtree_flutter_app/models/ebay_response.dart';
import 'package:gumtree_flutter_app/models/errors_response.dart';
import 'package:gumtree_flutter_app/models/item_response.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Networking {
  Future<SearchResponse> searchItems(String query) async {
    String _url = "https://api.sandbox.ebay.com/buy/browse/v1/item_summary/search?&q=$query";
    Response response = await http.get(
      _url,
      headers: headers(),
    );
    print("In networking: ${response.reasonPhrase}\nCode: ${response.statusCode}");

    print(response.body);
    print(json.decode(response.body));
    switch (response.statusCode) {
      case 200:
        return SearchResponse.fromJson(json.decode(response.body));
      default:
        ErrorsResponse errorsResponse = ErrorsResponse.fromJson(json.decode(response.body));
        SearchResponse searchResponse = SearchResponse();
        searchResponse.errors = errorsResponse.errors;
        return searchResponse;
    }
  }

  Map<String, String> headers() {
    return {
      HttpHeaders.authorizationHeader:
      "Bearer v^1.1#i^1#p^3#f^0#r^0#I^3#t^H4sIAAAAAAAAAOVYeWwUVRjv9uSwNTGeSMw6FIjA7M6x59hd6d1tabt2S8UqKW9n3rQDszPjzJt2N0ZTK4KAJkYBbfCoiVEJon"
          "+RUBFQExNNkBhiJTEajQmBWDVyeAf1zWxbtgWh29XQxE02u++97/x9xzuo/uK5yzY1bPql1FGSP9RP9ec7HPR8am5x0fKygvwFRXlUBoFjqL+8v3Cg4FSFARKyxrVBQ1MVAzqTCVkxOHsyRJi6wqnAkAxOAQlocIjnYpXNqzjGRXGariKVV2XCGakJEX4v42V9fpoXeH8QUF48q4zLbFdDhIcGHsoLBAowAgthEK8bhgkjioGAgkIEQzEUSTEk42unvRxDcR7axXo8nYSzA+qGpCqYxEURYdtczubVM2y9vKnAMKCOsBAiHKmsi7VWRmpqW9or3BmywmM4xBBApjF5VK0K0NkBZBNeXo1hU3Mxk+ehYRDucFrDZKFc5bgxMzDfhpr3iwxFCXSAhmmgcweyTtUTAF3eCmtGEkjRJuWggiSUuhKeGIv4esijsVELFhGpcVo/d5tAlkQJ6iGitqry3tWx2jbCGYtGdbVXEqBg+UmzHpbxBH0eIoyggQGEeleSpiwfxlSl5Y3BPEVXtaoIkgWa4WxRURXEdsOp6NAZ6GCiVqVVrxSRZVMmnX8CRbrTCmo6iibqUay4wgSGwmkPrxyD8ZS4kAT/VlJAMeAPYLBEH43/CfCitLBqfQapEbaiUxmNui1bYBykyATQN0CkyYCHJI/hNRNQlwSO9YoMGxAhKfiCIukJiiIZ9wo+khYhpCCMx/lg4P+VIQjpUtxEcCJLpi7YbuLQYVQ5CYgcUjdApT2lQWIqpd18xlIjaYSIHoQ0zu3u6+tz9bEuVe92445Au9c0r4rxPTABiAla6crEpGQnCY9zBtNzCBsQIpI4B7FypZsIt9XWtdXGGrraW5tqW8bzd5Jl4amz/+BpDPI6RLPLO7qzulFp7Aum2AYv3d0KJJlJKGxd3O2XkRaU6n33NHY3a/5ITO0O5eY8r2owqsoSn/qvEbBqPTsUWF2IAh2lqswUHsegLOOfnNw1LHdnV6gtfgMLAJrksorOxasJtwpw67amumyLndMhcsfNFNYvQN2lQyCoipyaPl+3iVtVmnt6TAaOhivddbEbWWqczJwFj6T04pal6qmZKJxgzoIH8LxqKmgm6sZYs+AQTVmUZNnqyjNRmMGejZkKkFNI4o2Zx9DedjG8htTdg2w5Vq1nIQvP4f0ay+ABArKabTpZCWz0qJpmZSKPO0YW9SKKuF6AydvHnOyMxRu+fd7M0tqL+XGXkOScpWg9qgJzlgIEQcf3hJzlWGfDnIWk7y4zqgVJsXqukU17wOchl6ADMZvq0UDKLldBMjRrm8lOXU57WaWmRRIJE4G4DCPC1dvUcK3XXHL3ZoN+xpuzi1fRs0ueO1NA6QWKQNabCaRD2EnGqtaQwBeE0OvxMCT+BvDFgsrJ7xrYO9v8xskcoOOMn2QDjEB6KD9FBgKAJsWAhwJe0e8RWCYnn6tlCRfS7LtoNKgGgsJ0XZsykXHLuuiK7Z78xhXOsz/0gOMgNeAYznc4LIzp5dQdxQWrCwuuIQzcMVwGTr24mnThm5kLb7cK7o86dG2AKQ1Ien6xQ/r8U/7XjNe1obXUzRPva3ML6PkZj23UwgsrRfS1N5ViYBjGR3sZCl8ZqUUXVgvpGwuvP1T+Udn+pwvVV4jjyYE7A8dvG/l5LVU6QeRwFOUVDjjyVuy56+OtoVtq5dbR+3Yx74D6in3nl28cfn7Owu/erVv8/V8b163ffXj0s7YnPkl8M+eRzi2urdeVuA+cG7zh7OO7tseTHSV7nlslfXtiECxNkfPQ/oLFraerDw4ve/brR596E/xEJUpWPLCVyme378gbevh805/HNp8uHVz7Ith7qLFzcfVo5LUlX5a8tHdB/pGSgGvw/Xld7MovRo82+7adGjkcK/yAPfLGM4/tE4rPPX7it9ub/ji2c1H5ks2dsZ4Hk3PeWnl0pPy9eStPrtvxZNMLr56kW7/6cWSQP9N31lsT3723OJpwDneAtofe3hnd0lfx++uBW39Yf2ZbeHWZtL1s877+l5dG2Q/PHbi/Ph3GvwGgVKct9xQAAA=="
    };
  }

  Future<dynamic> getNextSearchPageResults(String url) async {
    Response response = await http.get(
      url,
      headers: headers(),
    );
    print("In networking: ${response.reasonPhrase}\nCode: ${response.statusCode}");

    print(response.body);
    print(json.decode(response.body));
    switch (response.statusCode) {
      case 200:
        return SearchResponse.fromJson(json.decode(response.body));
      default:
        return SearchResponse();
    }
  }

  Future<dynamic> getItemById(String itemId) async {
    String url = "https://api.sandbox.ebay.com/buy/browse/v1/item/$itemId";
    Response response = await http.get(url, headers: headers());

    switch (response.statusCode) {
      case 200:
        return ItemResponse.fromJson(json.decode(response.body));
    }
  }
}
