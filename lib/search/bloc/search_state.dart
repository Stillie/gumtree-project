import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:gumtree_flutter_app/models/ebay_response.dart';

@immutable
abstract class SearchState extends Equatable {
  SearchState([List props = const []]) : super(props);
}

class InitialSearchState extends SearchState {}

class LoadingSearchState extends SearchState {}

class LoadingNextPageState extends SearchState {}

class CompleteLoadingNextPageState extends SearchState {
  final SearchResponse results;

  CompleteLoadingNextPageState({@required this.results}) : super([results]);
}

class CompleteSearchState extends SearchState {
  final SearchResponse results;

  CompleteSearchState({@required this.results}) : super([results]);
}

class EmptySearchState extends SearchState {}

class ErrorSearchState extends SearchState {
  final String errorMessage;
  final int errorCode;

  ErrorSearchState({@required this.errorMessage, @required this.errorCode});
}
