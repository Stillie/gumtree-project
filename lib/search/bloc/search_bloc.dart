import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:gumtree_flutter_app/models/ebay_response.dart';
import 'package:gumtree_flutter_app/networking/networking.dart';
import 'package:gumtree_flutter_app/search/bloc/search_event.dart';

import './bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  @override
  SearchState get initialState => InitialSearchState();

  Networking _networking = Networking();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is GetSearchResults) {
      yield LoadingSearchState();
      print('loading');
      final SearchResponse results = await _networking.searchItems(event.query);

      print(results.hasFailed);
      if (results.hasFailed) {
        yield ErrorSearchState(errorCode: results.errorCode, errorMessage: results.errorMessage);
      } else {
        if (results.itemSummaries == null || results.itemSummaries.isEmpty) {
          yield EmptySearchState();
        } else {
          print('done ${results.itemSummaries.toString()}');
          yield CompleteSearchState(results: results);
        }
      }
    }
    if (event is GetNextSearchResults) {
      yield LoadingNextPageState();
      final SearchResponse response = await _networking.getNextSearchPageResults(event.url);

      print(response.hasFailed);
      if (response.hasFailed) {
        yield ErrorSearchState(errorCode: response.errorCode, errorMessage: response.errorMessage);
      } else {
        print('done ${response.itemSummaries.toString()}');
        yield CompleteLoadingNextPageState(results: response);
      }
    }
  }
}
