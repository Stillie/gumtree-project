import 'package:equatable/equatable.dart';

abstract class SearchEvent extends Equatable {
  SearchEvent([List props = const []]) : super(props);
}

class GetSearchResults extends SearchEvent {
  final String query;

  GetSearchResults({this.query}) : super([query]);
}

class GetNextSearchResults extends SearchEvent {
  final String url;

  GetNextSearchResults({this.url}) : super([url]);
}
