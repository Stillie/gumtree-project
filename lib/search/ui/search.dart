import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gumtree_flutter_app/common/gumtree_app_bar.dart';
import 'package:gumtree_flutter_app/common/image_utils.dart';
import 'package:gumtree_flutter_app/item_details/ui/item_detail.dart';
import 'package:gumtree_flutter_app/models/item_summary.dart';
import 'package:gumtree_flutter_app/search/bloc/search_bloc.dart';
import 'package:gumtree_flutter_app/search/bloc/search_event.dart';
import 'package:gumtree_flutter_app/search/bloc/search_state.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String _query = "";
  List<ItemSummaries> _list = List();

  bool isLoading;
  String _next;

  _SearchPageState();

  final SearchBloc _searchBloc = SearchBloc();

  @override
  void dispose() {
    _searchBloc.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.position.pixels) {
        if (!isLoading) {
          isLoading = !isLoading;
          // Perform event when user reach at the end of list (e.g. do Api call)
          if (_next == null || _next.isEmpty) {
            return;
          }
          _searchBloc.add(GetNextSearchResults(url: _next));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GumtreeAppBar(title: "Search", child: _body());
  }

  Widget _body() {
    return BlocBuilder(
        bloc: _searchBloc,
        builder: (BuildContext context, SearchState state) {
          if (state is InitialSearchState) {
            return _buildInitial(_query);
          } else if (state is CompleteLoadingNextPageState) {
            return _buildComplete(state.results.itemSummaries);
          } else if (state is LoadingSearchState || state is LoadingNextPageState) {
            // show loading
            return _buildLoading();
          } else if (state is CompleteSearchState) {
            // show data
            print("Show data ${state.results.itemSummaries.length}");
            _next = state.results.next;
            return _buildComplete(state.results.itemSummaries);
          } else if (state is EmptySearchState) {
            // No results
            return _buildNoResults();
          } else if (state is ErrorSearchState) {
            final snackBar = SnackBar(content: Text(state.errorMessage));
            Scaffold.of(context).showSnackBar(snackBar);
            return _buildInitial(_query);
          } else {
            return _buildInitial(_query);
          }
        });
  }

  Widget _buildNoResults() {
    return Column(
      children: <Widget>[
        _buildInitial(_query),
        Center(
          child: Text("No results found for $_query"),
        ),
      ],
    );
  }

  Widget _buildLoading() {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.deepOrange,
      ),
    );
  }

  ScrollController _scrollController = ScrollController();

  Widget _buildComplete(List<ItemSummaries> results) {
    _list.addAll(results);
    return Column(
      children: <Widget>[
        _buildInitial(_query),
        Expanded(
          child: ListView.builder(
            itemCount: _list.length,
            itemBuilder: (context, index) {
              return PostWidget(post: _list[index]);
            },
          ),
        ),
      ],
    );
  }

  Widget _buildInitial(String value) {
    return Center(
      child: TextFormField(
        onFieldSubmitted: (String text) {
          _query = text;
          _searchBloc.add(GetSearchResults(query: text));
        },
        initialValue: value != null && value.isNotEmpty ? value : "",
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'What are you looking for?',
        ),
      ),
    );
  }
}

class PostWidget extends StatelessWidget {
  final ItemSummaries post;

  const PostWidget({Key key, @required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(post.title);
    String imageUrl = findAnImage(post);
    Widget image = (imageUrl.isNotEmpty)
        ? Hero(
      child: Image.network(imageUrl),
      tag: imageUrl,
    )
        : Icon(Icons.favorite);
    return ListTile(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ItemDetail(itemSummaries: post)),
        );
      },
      leading: image,
      title: Text('${post.title}'),
      isThreeLine: true,
      subtitle: Text("${post.price.currency} ${post.price.value}"),
      dense: true,
    );
  }
}
