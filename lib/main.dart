import 'package:flutter/material.dart';
import 'package:gumtree_flutter_app/common/constants.dart';

import 'search/ui/search.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gumtree Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: kGRAYISH_VIOLET,
        accentColor: kLIGHT_GRAYISH_ORANGE,
        primaryTextTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold, color: kGRAYISH_VIOLET),
          title: TextStyle(fontSize: 36.0,  color: Colors.white),
          body1: TextStyle(fontSize: 14.0, color: kGRAYISH_VIOLET),
        ),
      ),
      home: SearchPage(),
    );
  }
}
