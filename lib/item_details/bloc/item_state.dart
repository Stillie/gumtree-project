import 'package:equatable/equatable.dart';

abstract class ItemState extends Equatable {
  ItemState([List props = const []]) : super(props);
}

class InitialItemState extends ItemState {}

class LoadingItemState extends ItemState {}

class CompletedItemState extends ItemState {}
