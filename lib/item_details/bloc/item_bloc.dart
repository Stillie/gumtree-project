import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:gumtree_flutter_app/models/item_response.dart';
import 'package:gumtree_flutter_app/networking/networking.dart';

import './bloc.dart';

class ItemBloc extends Bloc<ItemEvent, ItemState> {
  @override
  ItemState get initialState => InitialItemState();
  Networking _networking = Networking();

  @override
  Stream<ItemState> mapEventToState(
    ItemEvent event,
  ) async* {
    // TODO: Add Logic
    if (event is GetItemById) {
      yield LoadingItemState();

      ItemResponse response = await _networking.getItemById(event.itemId);

      yield CompletedItemState();
    }
  }
}
