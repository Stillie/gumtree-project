import 'package:equatable/equatable.dart';

abstract class ItemEvent extends Equatable {
  ItemEvent([List props = const []]) : super(props);
}

class GetItemById extends ItemEvent {
  final String itemId;

  GetItemById(this.itemId) : super([itemId]);
}
