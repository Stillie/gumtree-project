import 'package:flutter/material.dart';
import 'package:gumtree_flutter_app/common/gumtree_app_bar.dart';
import 'package:gumtree_flutter_app/common/image_utils.dart';
import 'package:gumtree_flutter_app/item_details/bloc/bloc.dart';
import 'package:gumtree_flutter_app/models/item_summary.dart';

class ItemDetail extends StatefulWidget {
  final ItemSummaries itemSummaries;

  ItemDetail({Key key, this.itemSummaries}) : super(key: key);

  @override
  _ItemDetailState createState() => _ItemDetailState();
}

class _ItemDetailState extends State<ItemDetail> {
  ItemBloc _bloc = ItemBloc();
  ItemSummaries summaries;

  @override
  void dispose() {
    // TODO: implement dispose
    _bloc.close();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
//    _bloc.add(GetItemById(widget.itemSummaries.itemId));
    super.initState();
    summaries = widget.itemSummaries;
  }

  @override
  Widget build(BuildContext context) {
    return GumtreeAppBar(title: summaries.title, child: buildContainer());
  }

  Widget buildContainer() {
    String imageUrl = findAnImage(summaries);
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(child: Hero(tag: imageUrl, child: Image.network(imageUrl))),
            ],
          ),
          Text(summaries.title),
        ],
      ),
    );
  }
}
